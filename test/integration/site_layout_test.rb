require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  # testing the layout links 

  test 'layout links' do
    get root_path
    assert_template 'static_pages/home'
    #rails inserts the root_path value in place of the href
    # | 2 is for the number of root_path links. 1 for the navbar
    # brand and 2 for the home link
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path

  end 


end
